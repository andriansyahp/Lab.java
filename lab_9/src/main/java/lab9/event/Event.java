package lab9.event;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
* A class representing an event and its properties.
*/
public class Event {
    /** Desired format for the date */
    private static DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy, HH:mm:ss");

    /** Name of event */
    private String name;

    /** Instance variables for representing beginning and end time of event */
    private Date beginTime;
    private Date endTime;

    /** Instance variable for cost per hour */
    private BigInteger costPerHour;

    /**
     * Constructor for Event class.
     * @param name name of the event.
     * @param beginTime the beginning date and time representation of the event.
     * @param endTime the end date and time representation of the event.
     * @param costPerHour cost for attending the event (per hour).
     */
    public Event(String name, Date beginTime, Date endTime, BigInteger costPerHour) {
        this.name = name;
        this.beginTime = beginTime;
        this.endTime = endTime;
        this.costPerHour = costPerHour;
    }

    /**
     * Overloaded constructors accepting event as its parameters.
     * @param event source event to be passed
     */
    public Event(Event event) {
        this.name = new String(event.name);
        this.beginTime = constructNewDate(event.beginTime);
        this.endTime = constructNewDate(event.endTime);
        this.costPerHour = new BigInteger(String.valueOf(event.costPerHour));
    }

    /**
     * Accessor for costPerHour field.
     * @return the cost per hour of the event.
     */
    public BigInteger getCost() {
        return this.costPerHour;
    }

    /**
     * Accessor for beginTime field.
     * @return the date when the event starts.
     */
    public Date getStartDate() {
        return this.beginTime;
    }

    /**
    * Accessor for name field.
    * @return name of this event instance.
    */
    public String getName()
    {
        return this.name;
    }

    /**
     * Method for converting event instance into string to be printed in the screen.
     * @return the desired format for the string representation for the event instance.
     */
    public String toString() {
        String retStr = "";
        retStr += this.name + "\n";
        retStr += "Waktu mulai: " + dateFormat.format(this.beginTime) + "\n";
        retStr += "Waktu selesai: " + dateFormat.format(this.endTime) + "\n";
        retStr += "Biaya kehadiran: " + this.costPerHour;
        return retStr;
    }

    /**
     * Method to test if this event overlaps with another event.
     * @param anotherEvent another event to be compared
     * @return boolean operation for the overlap condition.
     */
    public boolean overlap(Event anotherEvent) {
        return (this.beginTime.before(anotherEvent.endTime)) && (this.endTime.after(anotherEvent.beginTime));
    }

    /**
     * Helper method to make a new Date object from another Date object type.
     * @param srcDate source date to be adapted.
     * @return the new date adapting the source date.
     */
    private Date constructNewDate(Date srcDate) {
        String srcDateStr = dateFormat.format(srcDate);
        Date newDate = null;
        try {
            newDate = dateFormat.parse(srcDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate;
    }

}
