package lab9;

import lab9.user.User;
import lab9.event.Event;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;

/**
* Class representing event managing system.
*/
public class EventSystem {
    /**
    * List of events.
    */
    private ArrayList<Event> events;

    /**
    * List of users.
    */
    private ArrayList<User> users;

    /**
    * Constructor. Initializes events and users with empty lists.
    */
    public EventSystem() {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }

    /**
     * Method for recording and instantiation of an event.
     * @param name the name of the event to be initiated.
     * @param startTimeStr the beginning date and time of the event.
     * @param endTimeStr the ending date and time of the event.
     * @param costPerHourStr the cost per hour for attending the event.
     * @return the String result of the instantiation.
     */
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr){
        if (findEvent(name) == null) {
            Date startTime = convertDateFormat(startTimeStr);
            Date endTime = convertDateFormat(endTimeStr);
            if (endTime.after(startTime)) {
                this.events.add(new Event(name, startTime, endTime, new BigInteger(costPerHourStr)));
                return String.format("Event %s berhasil ditambahkan!", name);
            }
            return "Waktu yang diinputkan tidak valid!";
        }
        return String.format("Event %s sudah ada!", name);
    }

    /**
     * Method to register a user from a given name.
     * @param name the name of the user to be registered.
     * @return the String result of the registration.
     */
    public String addUser(String name) {
        if (getUser(name) == null) {
            this.users.add(new User(name));
            return String.format("User %s berhasil ditambahkan!", name);
        }
        return String.format("User %s sudah ada!", name);
    }

    /**
     * Method for user planning to attend an event.
     * The plan result depends on several cases, such as:
     * no user with the given name found,
     * no event with the given name found,
     * or the event is overlapping with another event that the user already planned to attend earlier.
     * @param userName the name of the user that planning to attend the event.
     * @param eventName the name of the event that the user is planning to attend.
     * @return the plan result depending by the case in String.
     */
    public String registerToEvent(String userName, String eventName) {
        User user = getUser(userName);
        Event event = findEvent(eventName);
        if (user != null || event != null) {
            if (user != null) {
                if (event != null) {
                    if (user.addEvent(event)) {
                        return String.format("%s berencana menghadiri %s!", userName, eventName);
                    }
                    return String.format("%s sibuk sehingga tidak dapat menghadiri %s!", userName, eventName);
                }
                return String.format("Tidak ada acara dengan nama %s!", eventName);
            }
            return String.format("Tidak ada pengguna dengan nama %s!", userName);
        }
        return String.format("Tidak ada pengguna dengan nama %s dan acara dengan nama %s!", userName, eventName);
    }

    /**
     * Method to check whether an event exists or not.
     * @param eventName the name of the event to be searched.
     * @return the String representation of the event.
     */
    public String getEvent(String eventName) {
        Event event = findEvent(eventName);
        if (event != null) {
            return event.toString();
        }
        return String.format("Tidak ada acara dengan nama %s!", eventName);
    }

    /**
     * Method to check whether the user exists or not.
     * @param userName the name of the user to be searched.
     * @return the user found from the given name.
     */
    public User getUser(String userName) {
        for (User user : this.users) {
            if (user.getName().equals(userName)) {
                return user;
            }
        }
        return null;
    }

    /**
     * Helper method to check whether the event exists or not.
     * @param eventName the name of the event to be searched.
     * @return the event found from the given name.
     */
    private Event findEvent(String eventName) {
        for (Event event : this.events) {
            if (event.getName().equals(eventName)) {
                return event;
            }
        }
        return null;
    }

    /**
     * Helper method to convert a String type object to Date type object.
     * @param date the String form of the date to be converted.
     * @return the converted Date type object of the String form of date.
     */
    private Date convertDateFormat(String date) {
        DateFormat srcDf = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        Date currentDate = null;
        try {
            currentDate = srcDf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat newDf = new SimpleDateFormat("dd-MM-yyyy, HH:mm:ss");
        date = newDf.format(currentDate);
        Date newDate = null;
        try {
            newDate = newDf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate;
    }
}