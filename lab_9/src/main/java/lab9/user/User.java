package lab9.user;

import lab9.event.Event;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
* Class representing a user, willing to attend event(s).
*/
public class User implements Cloneable
{
    /** Name of user */
    private String name;

    /** List of events this user plans to attend */
    private ArrayList<Event> events;

    /**
    * Constructor
    * Initializes a user object with given name and empty event list
    */
    public User(String name)
    {
        this.name = name;
        this.events = new ArrayList<>();
    }

    /**
    * Accessor for name field
    * @return name of this instance
    */
    public String getName() {
        return name;
    }

    /**
    * Adds a new event to this user's planned events, if not overlapping
    * with currently planned events.
    *
    * @return true if the event if successfully added, false otherwise
    */
    public boolean addEvent(Event newEvent)
    {
        boolean isOverlap = false;
        for (Event event : this.events) {
            if (event.overlap(newEvent)) {
                isOverlap = true;
                break;
            }
        }
        if (!isOverlap) {
            this.events.add(newEvent);
            return true;
        }
        return false;
    }

    /**
    * Returns the list of events this user plans to attend,
    * Sorted by their starting time.
    * Note: The list returned from this method is a copy of the actual
    *       events field, to avoid mutation from external sources
    *
    * @return list of events this user plans to attend
    */
    public ArrayList<Event> getEvents()
    {
        ArrayList<Event> copied = copyEvents(this.events);
        Collections.sort(copied, Comparator.comparing(Event::getStartDate));
        return copied;
    }

    /**
     * Method to count the total cost for the user that willing to attend the events.
     * @return the total cost of attending the events.
     */
    public BigInteger getTotalCost() {
        BigInteger totalCost = new BigInteger("0");
        for (Event event : this.events) {
            totalCost = totalCost.add(event.getCost());
        }
        return totalCost;
    }

    /**
     * Helper method to make a copy of an actual event list.
     * @param origin the original source list to be copied.
     * @return the result of copied list.
     */
    private ArrayList<Event> copyEvents(ArrayList<Event> origin) {
        ArrayList<Event> copy = new ArrayList<>();
        for (Event event : origin) {
            copy.add(new Event(event));
        }
        return copy;
    }

}
