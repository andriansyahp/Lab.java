import employee.*;
import java.util.ArrayList;
import java.util.Arrays;

public class TampanCorps{
    ArrayList<Employee> employees = new ArrayList<Employee>();
    private static int SALARY_THRESHOLD;

    public void setSalaryThreshold(int salaryThresold) {
        SALARY_THRESHOLD = salaryThresold;
    }

    public Employee findEmployee(String name){
        for (Employee worker : employees) {
            if (worker.getName().equals(name)) {
                return worker;
            }
        }
        return null;
    }

    public String addEmployee(String worker, String position, int wage){
        if (employees.size() <= 10000) {
            if (findEmployee(worker) == null) {
                if (position.equalsIgnoreCase("manager")) {
                    Employee labor = new Manager(worker, wage);
                    employees.add(labor);
                } else if (position.equalsIgnoreCase("staff")) {
                    Employee labor = new Staff(worker, wage);
                    employees.add(labor);
                } else if (position.equalsIgnoreCase("intern")) {
                    Employee labor = new Intern(worker, wage);
                    employees.add(labor);
                } else {
                    return String.format("Tidak ada posisi jabatan kerja %s", position.toLowerCase());
                }
                return String.format("%s mulai bekerja sebagai %s di PT. TAMPAN\n", worker, position.toLowerCase());
            }
            return String.format("Karyawan dengan nama %s telah terdaftar\n", worker);
        }
        return "PT. Tampan sudah mencapai batas jumlah pegawai (10000 orang)";
    }

    public String status(String employee){
        Employee worker = findEmployee(employee);
        if (worker != null) {
            return String.format("%s %d\n", worker.getName(), worker.getWage());
        }
       return "Karyawan tidak ditemukan\n";
    }

    public String payday(){
        if (employees.size() > 0) {
            String retStr = "Semua karyawan telah diberikan gaji\n";
            for (Employee worker : employees) {
                worker.earnWage();
                if (readyForWageRise(worker)) {
                    retStr += wageRise(worker) + "\n";
                } if (readyForPromotion(worker)) {
                    retStr += promotion(worker) + "\n";
                }
            }
            return retStr;
        }
        return "PT. TAMPAN belum memiliki karyawan\n";

    }

    private boolean readyForPromotion(Employee worker) {
        String retVar = "";
        if (worker instanceof Staff) {
            if (worker.getWage() > SALARY_THRESHOLD) {
                return true;
            }
        }
        return false;
    }

    public String recruitSubordinate(String svName, String subordName){
        Employee supervisor = findEmployee(svName);
        Employee subordinate = findEmployee(subordName);
        if (supervisor != null && subordinate != null) {
            if (supervisor.canHaveSubordinate(subordinate)) {
                if (supervisor.getSubordinateList().size() < 10) {
                    supervisor.recruitSubordinate(subordinate);
                    return String.format("Karyawan %s berhasil ditambahkan menjadi bawahan %s\n", subordName, svName);
                }
                return ("Jumlah bawahan telah mencapai batas maksimal (10 orang");
            }
            return "Anda tidak layak memiliki bawahan\n";
        }
        return "Nama tidak berhasil ditemukan\n";
    }

    private String promotion(Employee worker) {
        ((Staff) worker).setPromoted(true);
        return String.format("Selamat, %s telah dipromosikan menjadi manager", worker.getName());
    }

    private boolean readyForWageRise(Employee worker) {
        if (worker.getSalaryCounter() >= 6 && worker.getSalaryCounter() % 6 == 0) {
            return true;
        }
        return false;
    }

    private String wageRise(Employee worker) {
        int currentWage = worker.getWage();
        int newWage = currentWage + currentWage * 10 / 100;
        worker.setWage(newWage);
        return String.format("%s mengalami kenaikan gaji sebesar 10%% dari %d menjadi %d", worker.getName(), currentWage, newWage);
    }

}
