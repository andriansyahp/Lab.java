import java.util.Scanner;
import java.util.Arrays;

class Lab8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        TampanCorps siangTampan = new TampanCorps();

        System.out.println("Selamat datang di Aplikasi SIANG-TAMPAN!");
        System.out.print("Masukkan batas gaji untuk promosi jabatan: ");
        int salaryThreshold = Integer.parseInt(input.nextLine());
        siangTampan.setSalaryThreshold(salaryThreshold);

        while (true) {
            System.out.print(">>> ");
            String[] command = input.nextLine().split(" ");
            if (command.length > 0) {
                if (command[0].equalsIgnoreCase("tambah_karyawan")) {
                    System.out.println(siangTampan.addEmployee(capitalize(command[1]), command[2], Integer.parseInt(command[3])));
                } else if (command[0].equalsIgnoreCase("status")) {
                    System.out.println(siangTampan.status(capitalize(command[1])));
                } else if (command[0].equalsIgnoreCase("tambah_bawahan")) {
                    System.out.println(siangTampan.recruitSubordinate(capitalize(command[1]), capitalize(command[2])));
                } else if (command[0].equalsIgnoreCase("gajian")) {
                    System.out.println(siangTampan.payday());
                } else if (command[0].equalsIgnoreCase("exit")) {
                    break;
                }
            } else {
                System.out.println("Perintah yang dimasukkan tidak sesuai");
            }
        }
    }

    private static String capitalize(String str) {
        String lowerStr = str.toLowerCase();
        String newStr = lowerStr.substring(0,1).toUpperCase() + lowerStr.substring(1);
        return newStr;
    }
}
