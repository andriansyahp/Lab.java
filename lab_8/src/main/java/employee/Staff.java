package employee;

public class Staff extends Manager {
    private boolean isPromoted;

    public Staff(String name, int wage) {
        super(name, wage);
    }

    public void setPromoted(boolean bool) {
        this.isPromoted = bool;
    }

    public boolean getPromoted(){
        return this.isPromoted;
    }

    @Override
    public void recruitSubordinate(Employee worker) {
        if (this.isPromoted) {
            super.recruitSubordinate(worker);
        } else {
            this.subordinateList.add(worker);
        }
    }

    @Override
    public boolean canHaveSubordinate(Employee worker) {
        if (this.isPromoted) {
            return super.canHaveSubordinate(worker);
        } else {
            if (worker instanceof Intern) {
                return true;
            }
            return false;
        }
    }
}
