package employee;

public class Manager extends Employee {
    public Manager(String name, int wage) {
        super(name, wage);
    }

    @Override
    public void recruitSubordinate(Employee worker) {
        this.subordinateList.add(worker);
    }

    @Override
    public boolean canHaveSubordinate(Employee worker) {
        if (worker instanceof Staff || worker instanceof Intern) {
            return true;
        }
        return false;
    }

}
