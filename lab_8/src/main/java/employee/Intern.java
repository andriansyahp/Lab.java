package employee;

public class Intern extends Employee{
    public Intern(String name, int wage) {
        super(name, wage);
    }

    @Override
    public boolean canHaveSubordinate(Employee worker) {
        return false;
    }

    @Override
    public void recruitSubordinate(Employee worker) {
        return;
    }
}
