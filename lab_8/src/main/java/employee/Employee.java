package employee;

import java.util.ArrayList;

public abstract class Employee {
    protected String name;
    protected int wage;
    protected int salaryCounter;
    protected ArrayList<Employee> subordinateList = new ArrayList<Employee>();

    public Employee(String name, int wage) {
        this.name = name;
        this.wage = wage;
    }

    public String getName() {
        return this.name;
    }

    public int getWage() {
        return this.wage;
    }

    public void setWage(int wage) {
        this.wage = wage;
    }

    public int getSalaryCounter() {
        return this.salaryCounter;
    }

    public ArrayList<Employee> getSubordinateList() {
        return this.subordinateList;
    }

    public void earnWage() {
        this.salaryCounter += 1;
    }

    public abstract void recruitSubordinate(Employee worker);
    public abstract boolean canHaveSubordinate(Employee worker);

}
