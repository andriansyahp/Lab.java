import java.util.Arrays;

public class BingoSetupNormal {
    private String[][] starterCards, newCards, transposedCards, diagonalCards;
    private boolean win = false;

    public BingoSetupNormal(String[][] cards) {
        this.starterCards = new String[5][5];
        this.newCards = new String[5][5];
        this.transposedCards = new String[5][5];
        this.diagonalCards = new String[2][5];
        for (int i = 0; i < cards.length; i++) {
            for (int j = 0; j < cards.length; j++) {
                this.starterCards[i][j] = cards[i][j];
                this.newCards[i][j] = cards[i][j];
            }
        }
    }

    public boolean isBingo() {
        return this.win;
    }

    private boolean checkWinner() {
        this.invertRowToColumn();
        for (int i = 0; i < 5; i++) {
            if (Arrays.stream(this.newCards[i]).distinct().count() == 1 ) {
                this.win = true;
                return this.win;
            } else {
                if (Arrays.stream(this.transposedCards[i]).distinct().count() == 1 ) {
                    this.win = true;
                    return this.win;
                }
            }
        }
        this.getDiagonals();
        for (int i = 0; i < 2; i++) {
            if (Arrays.stream(this.diagonalCards[i]).distinct().count() == 1 ) {
                this.win = true;
                return this.win;
            }
        }
        return this.win;
    }

    private void invertRowToColumn() {
        for (int row = 0; row < 5; row++) {
            for (int column = 0; column < 5; column++) {
                this.transposedCards[row][column] = this.newCards[column][row];
            }
        }
    }

    private void getDiagonals() {
        for (int i = -1, j = -1; i < 4 && j < 4; i++, j++) {
                this.diagonalCards[0][j+1] = this.newCards[i+1][j+1];
                this.diagonalCards[1][j+1] = this.newCards[i+1][3-j];
        }
    }

    public String info() {
        String infoPrint = "";
        for (int row = 0; row < 5; row++) {
            for (int column = 0; column < 5; column++) {
                if (this.newCards[row][column].length() > 1) {
                    infoPrint += String.format("| %s ", this.newCards[row][column]);
                } else infoPrint += String.format("| %s  ", this.newCards[row][column]);
            }
            if (row < 4) {
                infoPrint += "|\n";
            } else infoPrint += "|";
        }
        return infoPrint;
    }

    public String markNum(int num) {
        String number = Integer.toString(num);
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                if (this.newCards[i][j].equals(number)) {
                    this.newCards[i][j] = "X";
                    this.checkWinner();
                    return (number + " tersilang");
                } else {
                    if (this.starterCards[i][j].equals(number)) {
                        return (number + " sebelumnya sudah tersilang");
                    }
                }
            }
        }
        return ("Kartu tidak memiliki angka " + number);
    }

    public void restart() {
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                this.newCards[i][j] = this.starterCards[i][j];
            }
        }
        System.out.println("Mulligan!");
    }
}
