import java.util.Scanner;

public class BingoCard {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String[][] cards = new String[5][5];

        System.out.println("Welcome to 'BINGO!' game!");
        System.out.println("Masukkan 5 baris input berisi 5 kartu berupa angka, kartu pada tiap baris dipisahkan spasi:");
        for (int row = 0; row < 5; row++) {
            System.out.print("    >>> ");
            String rowOfCards = input.nextLine();
            String cardSplit[] = rowOfCards.split(" ");
            for (int column = 0; column < 5; column++) {
                 cards[row][column] = cardSplit[column];
            }
        }
        BingoSetupNormal player = new BingoSetupNormal(cards);

        while (true) {
            System.out.print("\n    >>> ");
            String gameInput = input.nextLine();
            if (gameInput.toUpperCase().equals("INFO")) {
                System.out.println(player.info());
            } else if (gameInput.toUpperCase().equals("RESTART")) {
                player.restart();
            } else {
                String[] gameInstruction = gameInput.split(" ");
                if (gameInstruction[0].toUpperCase().equals("MARK")) {
                    System.out.println(player.markNum(Integer.parseInt(gameInstruction[1])));
                    if (player.isBingo()) {
                        System.out.println("BINGO!\n");
                        System.out.println(player.info());
                        break;
                    }
                } else System.out.println("Incorrect command");
            }
        }
    }
}

