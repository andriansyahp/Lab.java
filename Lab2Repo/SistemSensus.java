import java.util.Scanner;

/**
 * Code Author (Mahasiswa):
 * @author Muhammad Andriansyah Putra, NPM 1706044093, Kelas F, GitLab Account: @andriansyahp
 */

public class SistemSensus {		
	public static void main(String[] args) {
		// Membuat input scanner baru
		Scanner input = new Scanner(System.in);

		// User Interface untuk meminta masukan
		System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
				"--------------------\n" +
				"Nama Kepala Keluarga   : ");
		String nama = input.nextLine();
		System.out.print("Alamat Rumah           : ");
		String alamat = input.nextLine();
		System.out.print("Panjang Tubuh (cm)     : ");
		int panjang = Integer.parseInt(input.nextLine());
		if (panjang <= 0 || panjang > 250) {
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0); 
		} 
		System.out.print("Lebar Tubuh (cm)       : ");
		int lebar = Integer.parseInt(input.nextLine());
		if (lebar <= 0 || lebar > 250) {
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0); 
		} 
		System.out.print("Tinggi Tubuh (cm)      : ");
		int tinggi = Integer.parseInt(input.nextLine());
		if (tinggi <= 0 || tinggi > 250) {
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0); 
		} 
		System.out.print("Berat Tubuh (kg)       : ");
		float berat = Float.parseFloat(input.nextLine());
		if (berat <= 0 || berat > 150) {
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Jumlah Anggota Keluarga: ");
		int jumlahAnggota = Integer.parseInt(input.nextLine());
		if (jumlahAnggota <= 0 || jumlahAnggota > 20) {
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Tanggal Lahir          : ");
		String tanggalLahir = input.nextLine();
		System.out.print("Catatan Tambahan       : ");
		String catatan = input.nextLine();
		System.out.print("Jumlah Cetakan Data    : ");
		int jumlahCetakan = Integer.parseInt(input.nextLine());
	
		// Menghitung rasio berat per volume (rumus lihat soal)
		int rasio = (int) (berat*1000000/((panjang)*(lebar)*(tinggi)));
		
		for (int halaman = 1; halaman < (jumlahCetakan + 1); halaman++) {
		// Meminta masukan terkait nama penerima hasil cetak data
			System.out.print("Pencetakan " + halaman + " dari " + jumlahCetakan + " untuk: ");
			String penerima = input.nextLine().toUpperCase(); // Lakukan baca input lalu langsung jadikan uppercase
			// Memeriksa ada catatan atau tidak
			String catatanAkhir = "";
			if (catatan.isEmpty()) {
				catatanAkhir = "Tidak ada catatan tambahan";
			} else {
				catatanAkhir = "Catatan: " + catatan;
			}

			// Mencetak hasil sesuai format 
			String hasil = String.format("\nDATA SIAP DICETAK UNTUK " + penerima + 
							"\n--------------------\n" + "%s - %s\n"
							+ "Lahir pada tanggal %s\n" + "Rasio berat per volume	= %d kg/m^3\n"
							+ "%s\n" , nama, alamat, tanggalLahir, rasio, catatanAkhir);
			System.out.println(hasil);
		}	
		
		// Menghitung nomor keluarga dari parameter yang telah disediakan sesuai rumus
		int hasilAscii = 0;
		for (int i = 0; i < nama.length(); i++) {
			hasilAscii += nama.charAt(i);
		}
		int hasilKalkulasi = ((panjang * lebar * tinggi) + hasilAscii) % 10000;

		// Menggabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
		String nomorKeluarga = nama.substring(0,1) + Integer.toString(hasilKalkulasi);
		// Menghitung anggaran makanan per tahun sesuai rumus 
		int anggaran = 50000 * 365 * jumlahAnggota;

		// Menghitung umur dari tanggalLahir sesuai rumus
		int tahunLahir = Integer.parseInt(tanggalLahir.substring(tanggalLahir.length()-4)); 
		int umur = 2018 - tahunLahir;

		// Melakukan proses menentukan apartemen sesuai kriteria
		String apartemen = "";
		String kabupaten = "";
		if (umur >= 0 && umur <= 18) {
			apartemen = "PPMT";
			kabupaten = "Rotunda";
		} else if (anggaran <= 100000000) {
			apartemen = "Teksas";
			kabupaten = "Sastra";
		} else if (anggaran > 100000000) {
			apartemen = "Mares";
			kabupaten = "Margonda";
		}

		// Mencetak rekomendasi sesuai format
		String rekomendasi = String.format("REKOMENDASI APARTEMEN\n--------------------\n" + "MENGETAHUI: %s - %s\n"
								+ "MENIMBANG: Anggaran makan tahunan: Rp %d\n\t   Umur kepala keluarga: %d tahun\n"
								+ "MEMUTUSKAN: keluarga %s akan ditempatkan di:\n%s, kabupaten %s", nama, nomorKeluarga,
								anggaran, umur, nama, apartemen, kabupaten);
		System.out.println(rekomendasi);
		
		input.close();
					
	}
}