package xoxo;

import xoxo.crypto.XoxoDecryption;
import xoxo.crypto.XoxoEncryption;
import xoxo.exceptions.InvalidCharacterException;
import xoxo.exceptions.KeyTooLongException;
import xoxo.exceptions.RangeExceededException;
import xoxo.exceptions.SizeTooBigException;
import xoxo.util.XoxoMessage;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.File;
import java.io.IOException;

/**
 * This class controls all the business
 * process and logic behind the program.
 *
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Muhammad Andriansyah Putra
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        gui.setVisible(true);

        ActionListener encryptListener = e -> encryptMessage();
        gui.setEncryptFunction(encryptListener);

        ActionListener decryptListener = e -> decryptMessage();
        gui.setDecryptFunction(decryptListener);
    }

    /**
     * Method to handle when the Encrypt button is pressed.
     * Encrypts the message, and then writes the results into
     * "encrypted.enc".
     */
    private void encryptMessage() {
        // Initiating essential variables to help lessen the typing.
        String success = "The encryption process is done successfully.";
        String fail = "The process is failed due to errors. Try Again.";
        String message = gui.getMessageText();
        String key = gui.getKeyText();
        String seed = gui.getSeedText();
        String fileName = "C:\\Users\\Andriansyah Putra\\Documents\\Universitas Indonesia\\Semester 2\\DDP 2\\Lab\\Lab1\\lab_10\\result";
        try {
            XoxoMessage encrypted;
            XoxoEncryption encr = new XoxoEncryption(key);
            // Condition if the user gives their own desired seed or using the default one.
            if (seed.length() == 0 ||
                    seed.equalsIgnoreCase("default_seed")) {
                seed = "DEFAULT_SEED";
                encrypted = encr.encrypt(message);
            }
            else {
                encrypted = encr.encrypt(message,
                                            Integer.parseInt(gui.getSeedText()));
            }
            String encryptedMsg = encrypted.getEncryptedMessage();
            String result = String.format("|%s|%s|%s|%s", message, key, seed, encryptedMsg);
            gui.appendLog(success + "..... " + encryptedMsg);

            // Writes the result to the desired file.
            FileWriter fw = new FileWriter(new File(fileName + "\\encrypted.enc"), true);
            fw.write(result +"\n");
            fw.close();

        // Handling exceptions through JOptionPane.
        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(gui, "The seed must be an integer.");
            gui.appendLog(fail);
        } catch (KeyTooLongException ex) {
            JOptionPane.showMessageDialog(gui, "The key is exceeding the permitted length.");
            gui.appendLog(fail);
        } catch (RangeExceededException ex) {
            JOptionPane.showMessageDialog(gui, "The seed is outside of the permitted range.");
            gui.appendLog(fail);
        } catch (SizeTooBigException ex) {
            JOptionPane.showMessageDialog(gui, "The size of the message is too big.");
            gui.appendLog(fail);
        } catch (InvalidCharacterException ex) {
            JOptionPane.showMessageDialog(gui, "The key is containing forbidden characters.");
            gui.appendLog(fail);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(gui, "Something is wrong with the file directory!");
        }
    }

    /**
     * Method to handle when the Decrypt button is pressed.
     * Decrypts the message, and then writes the results into
     * "decrypted.txt".
     */
    private void decryptMessage() {
        String success = "The decryption process is done successfully.";
        String fail = "The process is failed due to errors. Try Again.";
        String message = gui.getMessageText();
        String key = gui.getKeyText();
        String seed = gui.getSeedText();
        String fileName = "C:\\Users\\Andriansyah Putra\\Documents\\Universitas Indonesia\\Semester 2\\DDP 2\\Lab\\Lab1\\lab_10\\result";
        try {
            String decrypted;
            XoxoDecryption decr = new XoxoDecryption(gui.getKeyText());
            if (gui.getSeedText().length() == 0 ||
                    gui.getSeedText().equalsIgnoreCase("default_seed")) {
                decrypted = decr.decrypt(message, 18);
            } else {
                decrypted = decr.decrypt(message,
                        Integer.parseInt(gui.getSeedText()));
            }

            String result = String.format("|%s|%s|%s|%s", message, key, seed, decrypted);
            gui.appendLog(success + "..... " + decrypted);

            FileWriter fw = new FileWriter(new File(fileName + "\\decrypted.txt"), true);
            fw.write(result +"\n");
            fw.close();

        } catch (NumberFormatException ex) {
            JOptionPane.showMessageDialog(gui, "The seed must be an integer.");
            gui.appendLog(fail);
        } catch (KeyTooLongException ex) {
            JOptionPane.showMessageDialog(gui, "The key is exceeding the permitted length.");
            gui.appendLog(fail);
        } catch (RangeExceededException ex) {
            JOptionPane.showMessageDialog(gui, "The seed is outside of the permitted range.");
            gui.appendLog(fail);
        } catch (SizeTooBigException ex) {
            JOptionPane.showMessageDialog(gui, "The size of the message is too big.");
            gui.appendLog(fail);
        } catch (InvalidCharacterException ex) {
            JOptionPane.showMessageDialog(gui, "The key is containing forbidden characters.");
            gui.appendLog(fail);
        } catch (IOException e) {
            JOptionPane.showMessageDialog(gui, "Something is wrong with the file directory!");
        }
    }

}