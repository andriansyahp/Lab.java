package xoxo;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * This class handles most of the GUI construction.
 *
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Muhammad Andriansyah Putra
 */
public class XoxoView extends JFrame{

    /**
     * A field that used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField;

    /**
     * A field that used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField;

    /**
     * A field that used to be the input of the
     * desired seed. If it is left blank,
     * it will use the DEFAULT_SEED in HugKey class.
     */
    private JTextField seedField;

    /**
     * A field that used to display any log information such
     * as you click the button, an output file succesfully
     * created, etc.
     */
    private JTextArea logField;

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton;

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton;

    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        this.initGui();
    }

    /**
     * Constructs the GUI.
     */
    private void initGui() {
        // Making the title panel.
        // Also the title and its properties
        JPanel titlePanel = new JPanel(new BorderLayout());
        titlePanel.setLayout(new BorderLayout());
        titlePanel.setBackground(Color.BLACK);
        titlePanel.setPreferredSize(new Dimension(200, 200));
        titlePanel.setBorder(new EmptyBorder(50, 50, 50, 24));
        JLabel titleLabel1 = new JLabel("ENCRYPT - DECRYPT PROGRAM", JLabel.CENTER);
        titleLabel1.setForeground(Color.WHITE);
        titleLabel1.setFont(new Font("Impact", Font.BOLD, 48));
        JLabel titleLabel2 = new JLabel("(and vice versa)", JLabel.CENTER);
        titleLabel2.setForeground(Color.WHITE);
        titleLabel2.setFont(new Font("Palatino Linotype", Font.BOLD, 24));
        titlePanel.add(titleLabel1, BorderLayout.PAGE_START);
        titlePanel.add(titleLabel2, BorderLayout.PAGE_END);

        // Making the panel that will be holding labels.
        // Also making the labels and its properties as well
        JPanel labelPanel = new JPanel();
        labelPanel.setLayout(new BoxLayout(labelPanel, BoxLayout.Y_AXIS));
        labelPanel.setBackground(Color.BLACK);
        labelPanel.setPreferredSize(new Dimension(200, 100));
        // Making invisible gap between components
        labelPanel.setBorder(new EmptyBorder(16, 70, 16, 0));
        JLabel messageLabel = new JLabel("Message: ", JLabel.RIGHT);
        messageLabel.setForeground(Color.WHITE);
        messageLabel.setFont(new Font("Segoe UI", Font.PLAIN, 17));
        JLabel keyLabel = new JLabel("Key: ", JLabel.RIGHT);
        keyLabel.setForeground(Color.WHITE);
        keyLabel.setFont(new Font("Segoe UI", Font.PLAIN, 17));
        JLabel seedLabel = new JLabel("Seed: ", JLabel.RIGHT);
        seedLabel.setForeground(Color.WHITE);
        seedLabel.setFont(new Font("Segoe UI", Font.PLAIN, 17));
        labelPanel.add(messageLabel);
        // Creating gap between labels
        labelPanel.add(Box.createVerticalStrut(48));
        labelPanel.add(keyLabel);
        labelPanel.add(Box.createVerticalStrut(46));
        labelPanel.add(seedLabel);
        labelPanel.add(Box.createVerticalStrut(44));

        // Making the panel that will be holding the text fields.
        // Also making the text fields and all the properties.
        JPanel textFieldPanel = new JPanel();
        textFieldPanel.setLayout(new BoxLayout(textFieldPanel, BoxLayout.Y_AXIS));
        textFieldPanel.setBackground(Color.BLACK);
        textFieldPanel.setPreferredSize(new Dimension(900, 100));
        textFieldPanel.setBorder(new EmptyBorder(8, 24, 8, 24));
        messageField = new JTextField();
        messageField.setFont(new Font("Segoe UI", Font.PLAIN, 17));
        messageField.setPreferredSize(new Dimension(900, 40));
        // Preventing the text field to grow unexpectedly bigger to fill the gaps
        messageField.setMaximumSize(messageField.getPreferredSize());
        keyField = new JTextField();
        keyField.setFont(new Font("Segoe UI", Font.PLAIN, 17));
        keyField.setPreferredSize(new Dimension(900, 40));
        keyField.setMaximumSize(keyField.getPreferredSize());
        seedField = new JTextField();
        seedField.setFont(new Font("Segoe UI", Font.PLAIN, 17));
        seedField.setPreferredSize(new Dimension(900, 40));
        seedField.setMaximumSize(seedField.getPreferredSize() );
        textFieldPanel.add(messageField, BorderLayout.PAGE_START);
        textFieldPanel.add(Box.createVerticalStrut(30));
        textFieldPanel.add(keyField, BorderLayout.CENTER);
        textFieldPanel.add(Box.createVerticalStrut(30));
        textFieldPanel.add(seedField, BorderLayout.PAGE_END);
        textFieldPanel.add(Box.createVerticalStrut(30));

        // Making the panel that will be holding buttons.
        // Also making the buttons and all the properties.
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
        buttonPanel.setBackground(Color.BLACK);
        buttonPanel.setPreferredSize(new Dimension(200, 100));
        buttonPanel.setBorder(new EmptyBorder(35, 40, 55, 40));
        encryptButton = new JButton("Encrypt");
        encryptButton.setFont(new Font("Segoe Print", Font.BOLD, 17));
        encryptButton.setBackground(Color.ORANGE);
        encryptButton.setForeground(Color.BLACK);
        encryptButton.setBorder(new EmptyBorder(10, 5, 15, 5));
        decryptButton = new JButton("Decrypt");
        decryptButton.setFont(new Font("Segoe Print", Font.BOLD, 17));
        decryptButton.setBackground(Color.BLUE);
        decryptButton.setForeground(Color.WHITE);
        decryptButton.setBorder(new EmptyBorder(10, 5, 15, 5));
        buttonPanel.add(encryptButton, BorderLayout.PAGE_START);
        buttonPanel.add(Box.createVerticalStrut(20));
        buttonPanel.add(decryptButton, BorderLayout.PAGE_END);
        buttonPanel.add(Box.createVerticalStrut(20));

        // Making the panel that will be holding the log field.
        // Also initiated the logField, and configuring the properties.
        JPanel logPanel = new JPanel();
        logPanel.setBackground(Color.BLACK);
        logPanel.setPreferredSize(new Dimension(200, 250));
        logPanel.setBorder(new EmptyBorder(0, 250, 60, 12));
        logField = new JTextArea("Log field: \n",200, 100);
        // Making the text area uneditable.
        logField.setEditable(false);
        logField.setFont(new Font("Candara Bold", Font.ITALIC, 17));
        logField.setBackground(Color.BLACK);
        logField.setForeground(Color.WHITE);
        logPanel.add(logField);

        setTitle("(En)/(De)Crypt your message!");
        // Making the frame to go fullscreen.
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        add(titlePanel, BorderLayout.PAGE_START);
        add(labelPanel, BorderLayout.LINE_START);
        add(textFieldPanel, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.LINE_END);
        add(logPanel, BorderLayout.PAGE_END);

    }

    /**
     * Gets the message from the message field.
     *
     * @return The input message string.
     */
    public String getMessageText() {
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     *
     * @return The input key string.
     */
    public String getKeyText() {
        return keyField.getText();
    }

    /**
     * Gets the seed text from the seed field.
     *
     * @return The input seed string.
     */
    public String getSeedText() { return seedField.getText(); }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logField.append(log + '\n');
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     *
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */
    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     *
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }
}