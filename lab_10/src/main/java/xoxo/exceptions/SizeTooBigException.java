package xoxo.exceptions;

/**
 * An exception that is thrown if the message that
 * wants to be encrypted/decrypted is exceeding 10 Kbit
 * in size.
 *
 * @author Muhammad Andriansyah Putra
 */
public class SizeTooBigException extends RuntimeException{
    /**
     * Class constructor.
     */
    public SizeTooBigException(String message) {
        super(message);
    }

}