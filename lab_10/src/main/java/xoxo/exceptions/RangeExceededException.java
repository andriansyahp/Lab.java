package xoxo.exceptions;

/**
 * An exception that is thrown if the seed that
 * is used is not in the allowed range
 * (0-36).
 *
 * @author Muhammad Andriansyah Putra
 */
public class RangeExceededException extends RuntimeException {
    /**
     * Class constructor.
     */
    public RangeExceededException(String message) {
        super(message);
    }

}