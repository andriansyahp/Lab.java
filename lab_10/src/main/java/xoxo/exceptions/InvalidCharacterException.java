package xoxo.exceptions;

/**
 * An exception that is thrown if the key that
 * is used is containing forbidden letter
 * (Non-letters, or not an '@').
 *
 * @author Muhammad Andriansyah Putra
 */
public class InvalidCharacterException extends RuntimeException{
    /**
     * Class constructor.
     */
    public InvalidCharacterException(String message) {
        super(message);
    }

}