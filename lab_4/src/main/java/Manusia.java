public class Manusia {
    private String name;
    private int age, balance;
    private double happiness;

    public Manusia(String name, int age) {
        this.name = name;
        this.age = age;
        this.balance = 50000;
        this.happiness = 50;
    }

    public Manusia(String name, int age, int balance) {
        this.name = name;
        this.age = age;
        this.balance = balance;
        this.happiness = 50.0;
    }

    public Manusia(String name, int age, int balance, double happiness) {
        this.name = name;
        this.age = age;
        this.balance = balance;
        this.happiness = happiness;
    }

    public String getNama() {
        return this.name;
    }

    public int getUmur() {
        return this.age;
    }

    public int getUang() {
        return this.balance;
    }

    public double getKebahagiaan() {
        return this.happiness;
    }

    public void setUang(int balance) {
        this.balance = balance;
    }

    public void setKebahagiaan(double happiness) {
        if (happiness > 100) {
            this.happiness = 100;
        } else if (happiness < 0) {
            this.happiness = 0;
        } else this.happiness = happiness;
    }

    public void beriUang(Manusia receiver) {
        int sumOfChar = 0;
        String strName = receiver.getNama();
        for (int i = 0; i < strName.length(); i++) {
            sumOfChar += (int) strName.charAt(i);
        }
        int transferAmount = sumOfChar * 100;
        if ((this.balance - transferAmount) >= 0) {
            receiver.balance += transferAmount;
            this.balance -= transferAmount;
            double addHappiness = ((double) transferAmount / 6000);
            this.setKebahagiaan(this.getKebahagiaan() + addHappiness);
            receiver.setKebahagiaan(receiver.getKebahagiaan() + addHappiness);
            System.out.println(name +" memberi uang sebanyak " + transferAmount + " kepada " + receiver.getNama() + ", mereka berdua senang :D");
        } else System.out.println(name + " ingin memberi uang kepada " + receiver.getNama() + " namun tidak memiliki cukup uang :(");
    }

    public void beriUang(Manusia receiver, int transferAmount) {
        if ((this.balance - transferAmount) >= 0) {
            receiver.balance += transferAmount;
            this.balance -= transferAmount;
            double addHappiness = ((double) transferAmount / 6000);
            this.setKebahagiaan(this.getKebahagiaan() + addHappiness);
            receiver.setKebahagiaan(receiver.getKebahagiaan() + addHappiness);
            System.out.println(name +" memberi uang sebanyak " + transferAmount + " kepada " + receiver.getNama() + ", mereka berdua senang :D");
        } else System.out.println(name + " ingin memberi uang kepada " + receiver.getNama() + " namun tidak memiliki cukup uang :(");
    }

    public void bekerja(int duration, int workload) {
        if (this.age < 18) {
            System.out.println(this.getNama() + " belum boleh bekerja karena masih dibawah umur D:");
        } else {
            int totalWorkload = duration * workload;
            if (totalWorkload <= this.happiness) {
                int wage = totalWorkload * 10000;
                this.balance += wage;
                this.setKebahagiaan(this.getKebahagiaan() - totalWorkload);
                System.out.println(this.getNama() + " bekerja full time, total pendapatan : " + wage);
            } else {
                int newDuration = (int) (this.getKebahagiaan() / workload);
                totalWorkload = newDuration * workload;
                int wage = totalWorkload * 10000;
                this.balance += wage;
                this.setKebahagiaan(this.getKebahagiaan() - totalWorkload);
                System.out.println(this.getNama() + " tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : " + wage);
            }
        }
    }

    public void rekreasi(String destination) {
        int fee = destination.length() * 10000;
        if ((this.balance - fee) >= 0) {
            this.balance -= fee;
            int addHappiness = destination.length();
            this.setKebahagiaan(this.getKebahagiaan() + addHappiness);
            System.out.println(this.getNama() + " berekreasi di " + destination + ", " + this.getNama() + " senang :D");
        } else System.out.println(this.getNama() + " tidak mempunyai cukup uang untuk berekreasi di " + destination + " :(");
    }

    public void sakit(String disease) {
        int minusHappiness = disease.length();
        this.setKebahagiaan(this.getKebahagiaan() - minusHappiness);
        System.out.println(this.getNama() + " terkena penyakit " + disease + " :O");
    }

    public String toString() {
        String printInstance = ("Nama\t\t: " + this.getNama() + " \nUmur\t\t: " + this.getUmur() + "\nUang\t\t: " + this.getUang() + "\nKebahagiaan\t: " + this.getKebahagiaan());
        return printInstance;
    }
}


