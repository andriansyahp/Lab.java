package character;

import java.util.ArrayList;

//  write Player Class here
public class Player {
    protected String name;
    protected String type;
    protected int healthPts;
    protected boolean burned = false;
    protected boolean dead = false;
    protected ArrayList<Player> eatenPlayers = new ArrayList<Player>();


    public Player (String name, int healthPts, String type) {
        this.name = name;
        this.setHealthPts(healthPts);
        this.type = type;
    }

    public String getName() {
        return this.name;
    }

    public int getHp() {
        return this.healthPts;
    }

    public String getType() {
        return this.type;
    }

    public boolean getDead() {
        return this.dead;
    }

    public boolean getBurned() {
        return this.burned;
    }

    public ArrayList<Player> getDiet() {
        return this.eatenPlayers;
    }

    public void setHealthPts(int healthPts) {
        if (healthPts <= 0) {
            this.healthPts = 0;
            this.dead = true;
        } else {
            this.healthPts = healthPts;
        }
    }

    public void attack(Player target) {
        if (target.type.equals("Magician")) {
            target.setHealthPts(target.getHp() - 20);
        } else {
            target.setHealthPts(target.getHp() - 10);
        }
    }

    public boolean canEat(Player target) {
        if (this.type.equals("Human") || this.type.equals("Magician")) {
            if (target.type.equals("Human") || target.type.equals("Magician")) {
                return false;
            } else {
                if (target.burned) {
                    return true;
                }
                return false;
            }
        } else {
            if (target.type.equals("Human")) {
                return true;
            } else {
                if (target.dead) {
                    return true;
                }
                return false;
            }
        }
    }

    public void eat(Player target) {
        this.setHealthPts(this.getHp() + 15);
        target.setHealthPts(0);
        this.eatenPlayers.add(target);
    }



}
