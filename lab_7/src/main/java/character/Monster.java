package character;

//  write Monster Class here
public class Monster extends Player {
    String roarSound = "AAAAAAaaaAAAAAaaaAAAAAA";

    public Monster(String name, int healthPts) {
        super(name, healthPts*2, "Monster");
    }


    public Monster(String name, int healthPts, String type) {
        super(name, healthPts*2, type);
    }

    public Monster(String name, int healthPts, String roar, String type) {
        super(name, healthPts*2, type);
        this.roarSound = roar;
    }

    public String roar() {
        return this.roarSound;
    }

}
