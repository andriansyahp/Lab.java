package character;

//  write Magician Class here
public class Magician extends Human {
    public Magician(String name, int healthPts) {
        super(name, healthPts, "Magician");
    }
    public Magician(String name, int healthPts, String type) {
        super(name, healthPts, type);
    }

    public void burn(Player target) {
        if (target.type.equals("Magician")) {
            target.setHealthPts(target.getHp() - 20);
            if (target.dead) {
                target.burned = true;
            }
        } else {
            target.setHealthPts(target.getHp() - 10);
            if (target.dead) {
                target.burned = true;
            }
        }
    }

}
