package character;

//  write Human Class here
public class Human extends Player{
    public Human(String name, int healthPts) {
        super(name, healthPts, "Human");
    }

    public Human(String name, int healthPts, String type) {
        super(name, healthPts, type);
    }

}
