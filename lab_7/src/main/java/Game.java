import character.*;
import java.util.ArrayList;

public class Game{
    ArrayList<Player> player = new ArrayList<Player>();


    /**
     * Fungsi untuk mencari karakter
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name){
        for (Player chara : player) {
            if (chara.getName().equals(name)) {
                return chara;
            }
        }
        return null;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp){
        if (find(chara) == null) {
            if (tipe.equalsIgnoreCase("human")) {
                Player charact = new Human(chara, hp, "Human");
                player.add(charact);
            } else if (tipe.equalsIgnoreCase("monster")) {
                Player charact = new Monster(chara, hp, "Monster");
                player.add(charact);
            } else {
                Player charact = new Magician(chara, hp, "Magician");
                player.add(charact);
            }
            return String.format("%s ditambah ke game", chara);
        }
        return String.format("Sudah ada karakter bernama %s", chara);
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar){
        if (find(chara) != null) {
            if (tipe.equalsIgnoreCase("monster")) {
                Monster character = new Monster(chara, hp, roar, "Monster");
                player.add(character);
                return String.format("%s ditambah ke game", chara);
            }
            return String.format("%s bukan monster", chara);
        }
        return String.format("Sudah ada karakter bernama %s", chara);
    }

    /**
     * fungsi untuk menghapus character dari game
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara){
        if (find(chara) != null) {
            player.remove(find(chara));
            return String.format("%s dihapus dari game", chara);
        }
        return String.format("Tidak ada %s", chara);
    }


    /**
     * fungsi untuk menampilkan status character dari game
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String character){
        Player chara = find(character);
        if (chara != null) {
            String charaType = chara.getType();
            String retStr = String.format("%s %s\nHP: %d\n", charaType, chara.getName(), chara.getHp());
            if (!chara.getDead()) {
                retStr += "Masih hidup\n";
            } else {
                retStr += "Sudah meninggal dunia dengan damai\n";
            }
            if (chara.getDiet().size() > 0) {
                retStr += "Memakan ";
                for (Player eatenChara : chara.getDiet()) {
                    retStr += eatenChara.getType() + " " + eatenChara.getName();
                }
            } else {
                retStr += "Belum memakan siapa-siapa";
            }
            return retStr;
        }
       return String.format("Tidak ada %s", chara);
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status(){
        String retStr = "";
        for (Player chara : player) {
            retStr += status(chara.getName()) + "\n";
        }
        return retStr;
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara){
        String retStr = "";
        Player charact = find(chara);
        if (charact != null) {
            if (charact.getDiet().size() > 0) {
                for (Player eatenChara : charact.getDiet()) {
                    retStr += eatenChara.getType() + " " + eatenChara.getName();
                }
                return retStr;
            } else {
                return "Belum memakan siapa-siapa";
            }
        }
        return String.format("Tidak ada %s", chara);

    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet(){
        String retStr = "";
        for (Player chara : player) {
            retStr += diet(chara.getName()) + "\n";
        }
        return retStr;
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName){
        Player attacker = find(meName);
        Player attackTarget = find(enemyName);
        if (attacker != null) {
            if (attackTarget != null) {
                attacker.attack(attackTarget);
                return String.format("Nyawa %s %d", enemyName, attackTarget.getHp());
            }
            return String.format("Tidak ada %s", enemyName);
        }
        return String.format("Tidak ada %s", meName);

    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName){
        Player burner = find(meName);
        Player burnTarget = find(enemyName);
        if (burner != null) {
            if (burnTarget != null) {
                if (burner.getType().equals("Magician")) {
                    ((Magician) burner).burn(burnTarget);
                    String retStr = String.format("Nyawa %s %d", enemyName, burnTarget.getHp());
                    if (burnTarget.getBurned()) {
                        retStr += "\n dan matang";
                    }
                    return retStr;
                }
                return String.format("%s tidak bisa membakar", meName);
            }
            return String.format("Tidak ada %s", enemyName);
        }
        return String.format("Tidak ada %s", meName);
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName){
        Player predator = find(meName);
        Player prey = find(enemyName);
        if (predator != null) {
            if (prey != null) {
                if (predator.canEat(prey)) {
                    predator.eat(prey);
                    int predatorHP = predator.getHp();
                    player.remove(prey);
                    return String.format("%s memakan %s\nNyawa %s kini %d", meName, enemyName, meName, predatorHP);
                }
                return String.format("%s tidak bisa memakan %s", meName, enemyName);
            }
            return String.format("Tidak ada %s", enemyName);
        }
        return String.format("Tidak ada %s", meName);
    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName){
        Player charact = find(meName);
        if (charact != null) {
            if (charact.getType().equals("Monster")) {
                return ((Monster) charact).roar();
            }
            return String.format("%s tidak bisa berteriak", meName);
        }
        return String.format("Tidak ada %s", meName);
    }
}
