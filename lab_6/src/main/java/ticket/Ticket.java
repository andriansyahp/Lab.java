package ticket;
import movie.Movie;

public class Ticket {

    private Movie movie;
    private String dayOfShow, movieShowCategory;
    private boolean movieIs3D, dayOfShowIsWeekend;
    private int price;

    public Ticket(Movie movie, String dayOfShow, boolean movieIs3D) {
        this.movie = movie;
        this.setDayOfShow(dayOfShow);
        this.setMovieShowCategory(movieIs3D);
        this.setPrice();
    }

    public Movie getMovie() {
        return this.movie;
    }

    public String getDayOfShow() {
        return this.dayOfShow;
    }

    public int getPrice() {
        return this.price;
    }

    public String getMovieShowCategory() {
        return this.movieShowCategory;
    }

    public void setDayOfShow(String day) {
        this.dayOfShow = day;
        if (day.equals("Sabtu") || day.equals("Minggu")) {
            this.dayOfShowIsWeekend = true;
        } else {
            this.dayOfShowIsWeekend = false;
        }
    }

    public void setMovieShowCategory(boolean category) {
        this.movieIs3D = category;
        if (category) {
            this.movieShowCategory = "3 Dimensi";
        } else {
            this.movieShowCategory = "Biasa";
        }
    }

    public void setPrice() {
        if (this.dayOfShowIsWeekend) {
            if (this.movieIs3D) {
                this.price = 120000;
            } else this.price = 100000;
        } else {
            if (this.movieIs3D) {
                this.price = 72000;
            } else this.price = 60000;
        }
    }

    public String ticketInfo() {
        String line = ("------------------------------------------------------------------\n");
        String strInfo = String.format("Film\t\t\t: %s\nJadwal Tayang\t: %s\nJenis\t\t\t: %s\n", this.movie, this.dayOfShow, this.movieShowCategory);
        return line + strInfo + line;
    }
}
