package customer;
import movie.Movie;
import ticket.Ticket;
import theater.Theater;

public class Customer {

    private String name, gender;
    private int age;

    public Customer(String name, String gender, int age) {
        this.name = name;
        this.gender = gender;
        this.age = age;
    }

    public Ticket orderTicket(Theater theaterName, String movieTitle, String dayOfShow, String movieShowCategory) {
        Ticket orderedTicket = null;
        String title, showCategory;
        for (Ticket ticket : theaterName.getAvailableTickets()) {
            title = ticket.getMovie().getTitle();
            if (title.equals(movieTitle)) {
                if (ticket.getDayOfShow().equals(dayOfShow)) {
                    showCategory = ticket.getMovieShowCategory();
                    if (showCategory.equals(movieShowCategory)) {
                        if (this.age >= ticket.getMovie().getAgeRating()) {
                            theaterName.getAvailableTickets().remove(ticket);
                            System.out.printf("%s telah membeli tiket %s jenis %s di %s pada hari %s seharga Rp. %d\n", this.name, movieTitle, movieShowCategory, theaterName.getTheaterName(), dayOfShow, ticket.getPrice());
                            theaterName.setTheaterBalance(theaterName.getTheaterBalance() + ticket.getPrice());
                            orderedTicket = ticket;
                            return orderedTicket;
                       } else {
                            System.out.printf("%s masih belum cukup umur untuk menonton %s dengan rating %s\n", this.name, movieTitle, ticket.getMovie().getMovieRating());
                            return orderedTicket;
                        }
                    }
                }
            }
        }
        System.out.printf("Tiket untuk film %s jenis %s dengan jadwal %s tidak tersedia di %s\n", movieTitle, movieShowCategory, dayOfShow, theaterName.getTheaterName());
        return orderedTicket;
    }

    public void findMovie(Theater theaterName, String movieTitle) {
        for (Movie movie : theaterName.getMovieList()) {
            if (movie.getTitle().equals(movieTitle)) {
                movie.movieInfo();
                return;
            }
        }
        System.out.printf("Film %s yang dicari %s tidak ada di bioskop %s\n", movieTitle, this.name, theaterName.getTheaterName());
    }

}
