package theater;
import ticket.Ticket;
import movie.Movie;
import java.util.Arrays;
import java.util.ArrayList;

public class Theater {

    private String theaterName;
    private ArrayList<Ticket> availableTickets;
    private Movie[] movieList;
    private int theaterBalance;

    public Theater(String theaterName, int theaterBalance, ArrayList<Ticket> availableTickets, Movie[] movieList) {
        this.theaterName = theaterName;
        this.theaterBalance = theaterBalance;
        this.availableTickets = availableTickets;
        this.movieList = movieList;
    }

    public String getTheaterName() {
        return this.theaterName;
    }

    public ArrayList<Ticket> getAvailableTickets() {
        return this.availableTickets;
    }

    public Movie[] getMovieList() {
        return this.movieList;
    }

    public int getTheaterBalance() {
        return this.theaterBalance;
    }

    public void setTheaterBalance(int theaterBalance) {
        this.theaterBalance = theaterBalance;
    }

    public void setAvailableTickets(ArrayList<Ticket> availableTickets) {
        this.availableTickets = availableTickets;
    }

    public static void printTotalRevenueEarned(Theater[] theaters) {
        int totalRevenue = 0;
        String line = ("------------------------------------------------------------------");
        String revenuePrint = "\n";
        for (int i = 0; i < theaters.length; i++) {
            totalRevenue += theaters[i].theaterBalance;
            revenuePrint += String.format("Bioskop\t\t: %s\nSaldo Kas\t: %d", theaters[i].theaterName, theaters[i].theaterBalance);
            if (i < theaters.length - 1) {
                revenuePrint += "\n\n";
            } else {
                revenuePrint += "\n";
            }
        }
        System.out.printf("Total uang yang dimiliki Koh Mas : Rp. %d\n", totalRevenue);
        System.out.println(line + revenuePrint + line);
    }

    public void printInfo() {
        String line = ("------------------------------------------------------------------");
        String strInfo = String.format("\nBioskop \t\t: %s\nSaldo Kas   \t\t: %d\nJumlah tiket tersedia   : %d\nDaftar Film tersedia\t: ", this.theaterName, this.theaterBalance, this.availableTickets.size());
        for (int movies = 0; movies < this.movieList.length; movies++) {
            strInfo += this.movieList[movies].getTitle();
            if (movies == movieList.length - 1) {
                strInfo += "\n";
            } else {
                strInfo += ", ";
            }
        }
        System.out.println(line + strInfo + line);
    }
}
