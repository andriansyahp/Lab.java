package movie;

public class Movie {

    private String title, genre, origin, movieRating;
    private int duration, ageRating;

    public Movie(String title, String rate, int duration, String genre, String origin) {
        this.title = title;
        this.duration = duration;
        this.genre = genre;
        this.origin = origin;
        this.setAgeRating(rate);
    }

    public String getTitle() {
        return this.title;
    }

    public int getAgeRating() {
        return this.ageRating;
    }

    public String getMovieRating() {
        return this.movieRating;
    }

    public void setAgeRating(String rate) {
        this.movieRating = rate;
        if (rate.equals("Dewasa")) {
            this.ageRating = 17;
        } else if (rate.equals("Remaja")) {
            this.ageRating = 13;
        } else this.ageRating = 0;
    }

    public void movieInfo() {
        String line = ("------------------------------------------------------------------");
        String strInfo = String.format("\nJudul   : %s\nGenre   : %s\nDurasi  : %d menit\nRating  : %s\nJenis   : Film %s\n", this.title, this.genre, this.duration, this.movieRating, this.origin);
        System.out.println(line + strInfo + line);
    }
}
