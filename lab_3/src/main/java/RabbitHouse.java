import java.util.Scanner;

public class RabbitHouse{

    public static int permutate(int permutateSum) {
        if (permutateSum == 1) {
            return 1;
        } else {
            return (permutateSum * (permutate(permutateSum - 1))) + 1;
        }
    }

    public static boolean isPalindrome(String checkName) {
        if (checkName.length() <= 1) {
            return true;
        } else if (checkName.charAt(0) == checkName.charAt(checkName.length() - 1)) {
                return isPalindrome(checkName.substring(1, checkName.length() - 1));
        } else {
            return false;
        }
    }

    public static int palindrome(String palindromeName) {
        int counter = 0;
        if (isPalindrome(palindromeName)) {
            return 0;
        } else {
            counter += 1;
            for (int i = 0; i < palindromeName.length(); i++) {
                counter += palindrome(palindromeName.substring(0, i) + palindromeName.substring(i + 1, palindromeName.length()));
            }
            return counter;
        }

    }

    public static void main(String[] args) {
        Scanner scanInput = new Scanner(System.in);
        System.out.print("Masukkan nama kelinci beserta mode yang diinginkan\nFormat masukan: [mode] [nama]\n\n>>> ");
        String userInput = scanInput.nextLine();
        String[] splitInput = userInput.split(" ");
        String mode = splitInput[0];
        String name = splitInput[1];
        if (mode.equalsIgnoreCase("normal")) {
            System.out.println("\nJumlah kelinci: " + permutate(name.length()));
        } else {
            System.out.println("\nJumlah kelinci: " + palindrome(name) + "\n");
        }
    }

}
